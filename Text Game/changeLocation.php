<?php
  // Change player location
function changeLocation(){	
  global $location;
  echo "Where do you want to go?\n";
  $destination = strtolower(readline(">> "));
  
  if ($location === "kitchen" && $destination === "bathroom") {
    echo "You go to: bathroom.\n";
    $location = "bathroom";
  } elseif ($location === "kitchen" && $destination === "woods"){
    echo "You follow the winding path, shivering as you make your way deep into the Terror Woods.\n";
    $location = "woods";
  } elseif ($location === "bathroom" && $destination === "kitchen"){
    echo "You go to: kitchen.\n";
    $location = "kitchen";
  } elseif ($location === "woods" && $destination === "kitchen"){
    echo "You go to: kitchen.\n";
    $location = "kitchen";
  } elseif ($destination === "woods" || $destination === "kitchen" || $destination === "bathroom") {
    echo "You can't go directly to there from your current location. Try going somewhere else first.\n";
  } else {
    echo "That doesn't make sense. Are you confused? Try 'look around'.\n";
  }
  
}
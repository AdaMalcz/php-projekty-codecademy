<?php

$play_count = 0;
$correct_guesses = 0;
$guess_high = 0;
$guess_low = 0;

function guessNumber() 
{
  global $guess_high, $guess_low, $correct_guesses, $play_count;
  $play_count++;
  $number = rand(1, 10);
  echo "\nMake your guess...\n";
  $user_guess = intval(readline(">> "));
  echo "Round: $play_count\nMy Number: $number\nYour guess: $user_guess";
  if ($number === $user_guess)
  {
    $correct_guesses++;
  } elseif ($number < $user_guess) {
    $guess_high++;
  } else {
    $guess_low++;
  }
}

echo "I'm going to think of numbers between 1 and 10 (inclusive). Do you think you can guess correctly?\n";
  guessNumber();
  guessNumber();
  guessNumber();
  guessNumber();
  guessNumber();
  guessNumber();
  guessNumber();
  guessNumber();
  guessNumber();
  guessNumber();
    
  $percentage_right = $correct_guesses / ($correct_guesses + $guess_high + $guess_low) * 100;

echo "\nAfter $play_count rounds, here are some facts about your guessing:\nYou guessed the number correctly $percentage_right% of the time.\n";
if ($guess_high > $guess_low){
    echo "When you guessed wrong, you tended to guess high";
} else if ($guess_high < $guess_low) {
    echo "When you guessed wrong, you tended to guess low";
}
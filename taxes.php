<?php

// EXPENSES ARRAYS
$annualExpenses = [
    "vacations" => 1000,
    "carRepairs" => 1000,    
];

$monthlyExpenses = [
    "rent" => 1500,
    "utilities" => 200,
    "healthInsurance" => 200
];

$weeklyExpenses = [
    "gas" => 25,
    "food" => 90,
    "entertainment" => 47
];

// monthly salary (gross)
$grossSalary = 48150;
$socialSecurity = $grossSalary * .062;

// segments of progressive tax system 
$incomeSegments = [[9700, .88], [29775, .78], [8675, .76]]; 

// annual income (net) including taxes segments:
$netIncome = ($incomeSegments[0][0] * $incomeSegments[0][1]) + ($incomeSegments[1][0] * $incomeSegments[1][1]) + ($incomeSegments[2][0] * $incomeSegments[2][1]);

// annual income after social security subtraction
$annualIncome = $netIncome - $socialSecurity;

// annual income after annual expenses
$annualIncome -= ($annualExpenses['vacations'] + $annualExpenses['carRepairs']);
echo "Annual income: $annualIncome";
echo "\n";
  
// monthly income
$monthlyIncome = $annualIncome / 12;

// monthly income after monhly expenses
$monthlyIncome -= ($monthlyExpenses['rent'] + $monthlyExpenses['utilities'] + $monthlyExpenses['healthInsurance']);
echo "Monthly income: $monthlyIncome";
echo "\n";

// weekly income
$weeklyIncome = $monthlyIncome / 4.33;

// weekly income after weekly expenses
$weeklyIncome -= ($weeklyExpenses['gas'] + $weeklyExpenses['food'] + $weeklyExpenses['entertainment']);
echo "Weekly income: $weeklyIncome";
echo "\n";
  
// savings after a year
$savings = $weeklyIncome * 52;
echo "Total savings: $savings";

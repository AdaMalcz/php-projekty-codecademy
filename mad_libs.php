<?php

function generateStory($singular_noun, $verb, $color, $distance_unit)
{
  $story = "\n
The ${singular_noun}s are lovely, $color, and deep.
But I have promises to keep,
And ${distance_unit}s to go before I $verb,
And ${distance_unit}s to go before I $verb.\n";
  
  return $story;
}

echo generateStory("road", "read", "red", "kilometer");
echo generateStory("geek", "grow", "green", "pixel");
echo generateStory("black guy", "block", "black", "dead");

<?php

function eatSoup(){
  global $has_soup, $is_hungry;
  
  if (!$has_soup) {
    echo "You don't have any cooked food to eat!\n";
  } else {
    $is_hungry = FALSE;
    $has_soup = FALSE;
    echo "You have eaten the soup!\n";
  }
  
}